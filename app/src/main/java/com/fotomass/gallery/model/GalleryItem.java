package com.fotomass.gallery.model;

/**
 * Created by svilen on 19.12.2017 г..
 */

public class GalleryItem {

    private final int imageId;

    private final String title;

    GalleryItem(int imageId, String title) {
        this.imageId = imageId;
        this.title = title;
    }

    public int getImageId() {
        return imageId;
    }

    public String getTitle() {
        return title;
    }
}
