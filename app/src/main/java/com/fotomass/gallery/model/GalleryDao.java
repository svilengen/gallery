package com.fotomass.gallery.model;

import java.util.ArrayList;
import java.util.List;

import com.fotomass.gallery.R;


/**
 * Created by svilen on 19.12.2017 г..
 */

public class GalleryDao {


    private int[] getImageIds()

    {
        int[] ids = new int[]{R.drawable.img1, R.drawable.img2, R.drawable.img3, R.drawable.img4, R.drawable.img5, R.drawable.img6, R.drawable.img7, R.drawable.img8,
                R.drawable.img9, R.drawable.img10, R.drawable.img11, R.drawable.img12, R.drawable.img14,R.drawable.img15,
                R.drawable.img16,R.drawable.img17,R.drawable.img18,R.drawable.img19,R.drawable.img20};
        return ids;
    }


    public List<GalleryItem> getItems() {

        List<GalleryItem> items = new ArrayList<>();


        for (int id : getImageIds()) {
            items.add(new GalleryItem(id, "title" + id));
        }

        return items;
    }
}
