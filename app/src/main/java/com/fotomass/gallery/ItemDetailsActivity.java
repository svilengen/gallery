package com.fotomass.gallery;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

public class ItemDetailsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_details);


        int imgId = getIntent().getIntExtra("img", 0);

        ImageView imgView = findViewById(R.id.imageView);
        Picasso.with(getApplicationContext()).load(imgId).into(imgView);
    }
}
