package com.fotomass.gallery;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.fotomass.gallery.model.GalleryItem;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by svilen on 19.12.2017 г..
 */

class GalleryAdapter extends RecyclerView.Adapter<GalleryAdapter.ViewHolder>{
    private final List<GalleryItem> items;
    private final Context context;

    public GalleryAdapter(Context applicationContext, List<GalleryItem> items) {
        this.items = items;
        this.context = applicationContext;
    }

    @Override
    public GalleryAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_layout, parent, false);
        return new ViewHolder(view);
      }

    @Override
    public void onBindViewHolder(GalleryAdapter.ViewHolder holder, int position) {
        final GalleryItem item = items.get(position);

        holder.title.setText(item.getTitle());
        holder.img.setScaleType(ImageView.ScaleType.FIT_XY);

        Picasso.with(context).load(item.getImageId()).resize(640, 480).into(holder.img);


        holder.img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(v.getContext(), ItemDetailsActivity.class);
                intent.putExtra("img",item.getImageId());


                v.getContext().startActivity(intent);

            }
        });

    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView title;
        private ImageView img;
        private Context context;
        public ViewHolder(View view) {
            super(view);

            context = view.getContext();

            title = (TextView)view.findViewById(R.id.title);
            img = (ImageView) view.findViewById(R.id.img);
        }
    }
}
