package com.fotomass.gallery;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.fotomass.gallery.model.GalleryDao;
import com.fotomass.gallery.model.GalleryItem;

import java.util.List;

public class GalleryActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gallery);

        RecyclerView recyclerView = findViewById(R.id.imagegallery);



       RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getApplicationContext(),3);


        recyclerView.setLayoutManager(layoutManager);

        recyclerView.setHasFixedSize(true);
        recyclerView.setItemViewCacheSize(5);
        recyclerView.setDrawingCacheEnabled(true);
        recyclerView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);


        List<GalleryItem> items = new GalleryDao().getItems();

           GalleryAdapter adapter = new GalleryAdapter(getApplicationContext(), items);
           adapter.setHasStableIds(true);
        recyclerView.setAdapter(adapter);
    }
}
